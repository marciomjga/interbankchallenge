Oi!

Esse é meu code challenge pro Banco Inter!!
Foi muito interessante e desafiador pensar e fazer uma aplicação do início ao fim!

Esses são os passos requisitados para o desafio:

#1: Executando a aplicação

Caso estaja usando o Windows, ao entrar no diretório raíz e executar o seguinte comando:

``mvnw spring-boot:run``

Caso use MAC ou Linux:

``./mvnw spring-boot:run``


#2 Executando testes

Caso estaja usando o Windows, ao entrar no diretório raíz e executar o seguinte comando:

``mvnw test``

Caso use MAC ou Linux:

``./mvnw test``

#3 Contatos

Caso haja qualquer dúvida fique à vontade pra me contactar pelo meu email ``mjga.2008@gmail.com`` .

Have fun!! :) 

