package com.challenge.interbank.singleDigit.controller;

import com.challenge.interbank.singleDigit.SingleDigitApplication;
import com.challenge.interbank.singleDigit.dtos.UserDto;
import com.challenge.interbank.singleDigit.model.ModelApiResponse;
import com.challenge.interbank.singleDigit.model.User;
import com.challenge.interbank.singleDigit.services.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = { SingleDigitApplication.class })
@AutoConfigureMockMvc
public class UserApiControllerTest {

	private static final String POST_RESOURCE_URI = "/singledigit/v1/users";
	private static final String GET_BY_ID_RESOURCE_URI = "/singledigit/v1/users/1";
	private static final String GET_BY_USERNAME_RESOURCE_URI = "/singledigit/v1/users/findByName/username";
	private static final String PUT_RESOURCE_URI = "/singledigit/v1/users/{userId}";
	private static final Long ID = Long.valueOf(1);
	private static final String USERNAME = "username";
	private static final String EMAIL = "email@email.com.br";

	@LocalServerPort
	private int port;

	@MockBean
	private UserService userService;

	@Autowired
	private TestRestTemplate restTemplate;

	private User user;
	private HttpHeaders headers;

	@Before
	public void setUp() {
		headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		user = new User(ID, USERNAME, EMAIL);
	}

	@Test
	public void whenCreateUser_shouldPersistDataAndReturnStatusCode201() throws JsonProcessingException {
		
		given(userService.save(Mockito.any(User.class))).willReturn(user);

		HttpEntity<String> entity = createHttpEntity(new UserDto().withUsername(USERNAME).withEmail(EMAIL));

		ResponseEntity<ModelApiResponse<User>> response = restTemplate.exchange(createURLWithPort(POST_RESOURCE_URI),
		HttpMethod.POST, entity, new ParameterizedTypeReference<ModelApiResponse<User>>(){});

		assertThat(response.getStatusCodeValue()).isEqualTo(201);

	}

	@Test
	public void createUserWhenUsernameIsNull_shouldReturnStatusCode400BadRequest() throws JsonProcessingException {
		
		given(userService.save(Mockito.any(User.class))).willReturn(user);

		HttpEntity<String> entity = createHttpEntity(new UserDto().withUsername(USERNAME));

		ResponseEntity<ModelApiResponse<User>> response = restTemplate.exchange(createURLWithPort(POST_RESOURCE_URI),
		HttpMethod.POST, entity, new ParameterizedTypeReference<ModelApiResponse<User>>(){});

		assertThat(response.getStatusCodeValue()).isEqualTo(400);

	}

	@Test
	public void getUserById_shouldReturnStatusCode200() {
		
		given(userService.findById(Mockito.anyLong())).willReturn(Optional.ofNullable(user));
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		
		ResponseEntity<ModelApiResponse<User>> response = restTemplate.exchange(createURLWithPort(GET_BY_ID_RESOURCE_URI),
		HttpMethod.GET, entity, new ParameterizedTypeReference<ModelApiResponse<User>>(){});
		
		assertThat(response.getStatusCodeValue()).isEqualTo(200);
	}
	
	@Test
	public void getUserByIdWhenUserNotFound_shouldReturnStatusCode404() {
			
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		
		ResponseEntity<ModelApiResponse<User>> response = restTemplate.exchange(createURLWithPort(GET_BY_ID_RESOURCE_URI),
		HttpMethod.GET, entity, new ParameterizedTypeReference<ModelApiResponse<User>>(){});
		
		assertThat(response.getStatusCodeValue()).isEqualTo(404);
	}
	
	@Test
	public void getUserByUsername_shouldReturnStatusCode200() {
		
		given(userService.findByUsername(Mockito.anyString())).willReturn(Optional.ofNullable(user));
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		
		ResponseEntity<ModelApiResponse<User>> response = restTemplate.exchange(createURLWithPort(GET_BY_USERNAME_RESOURCE_URI),
		HttpMethod.GET, entity, new ParameterizedTypeReference<ModelApiResponse<User>>(){});
		
		assertThat(response.getStatusCodeValue()).isEqualTo(200);
	}
	
	@Test
	public void getUserByUsernameWhenUserNotFound_shouldReturnStatusCode404() {
			
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		
		ResponseEntity<ModelApiResponse<User>> response = restTemplate.exchange(createURLWithPort(GET_BY_USERNAME_RESOURCE_URI),
		HttpMethod.GET, entity, new ParameterizedTypeReference<ModelApiResponse<User>>(){});
		
		assertThat(response.getStatusCodeValue()).isEqualTo(404);
	}
	
	@Test
	public void whenUpdateUser_shouldReturnStatusCode200() throws JsonProcessingException {
		given(userService.findById(Mockito.anyLong())).willReturn(Optional.ofNullable(user));
		given(userService.update(Mockito.any(User.class))).willReturn(user);
		
		HttpEntity<String> entity = createHttpEntity(new UserDto().withUsername(USERNAME).withEmail(EMAIL));

		ResponseEntity<ModelApiResponse<User>> response =
				restTemplate.exchange(createURLWithPort(PUT_RESOURCE_URI),
																HttpMethod.PUT, entity, new ParameterizedTypeReference<ModelApiResponse<User>>(){}, 1L);
		
		assertThat(response.getStatusCodeValue()).isEqualTo(200);
	}
	
	@Test
	public void whenUpdateUser_shouldReturnStatusCode404() throws JsonProcessingException {
		
		HttpEntity<String> entity = createHttpEntity(new UserDto().withUsername(USERNAME).withEmail(EMAIL));

		ResponseEntity<ModelApiResponse<User>> response =
				restTemplate.exchange(createURLWithPort(PUT_RESOURCE_URI),
																HttpMethod.PUT, entity, new ParameterizedTypeReference<ModelApiResponse<User>>(){}, 1L);
		
		assertThat(response.getStatusCodeValue()).isEqualTo(404);
	}

	private HttpEntity<String> createHttpEntity(UserDto userDto) throws JsonProcessingException {
		HttpEntity<String> entity = new HttpEntity<String>(new ObjectMapper().writeValueAsString(userDto), headers);
		return entity;
	}

	private String createURLWithPort(String uri) {
		return new StringBuffer()
				.append("http://localhost:")
				.append(port)
				.append(uri)
				.toString();
	}

}
