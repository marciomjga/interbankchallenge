package com.challenge.interbank.singleDigit.controller;

import com.challenge.interbank.singleDigit.SingleDigitApplication;
import com.challenge.interbank.singleDigit.dtos.SingleDigitDto;
import com.challenge.interbank.singleDigit.model.ModelApiResponse;
import com.challenge.interbank.singleDigit.model.User;
import com.challenge.interbank.singleDigit.services.SingleDigitService;
import com.challenge.interbank.singleDigit.services.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = { SingleDigitApplication.class })
@AutoConfigureMockMvc
public class SingleDigitApiControllerTest {

	private static final String POST_RESOURCE_URI = "/singledigit/v1/calculate/{userId}";
	private static final Long ID = Long.valueOf(1);
	private static final String USERNAME = "username";
	private static final String EMAIL = "email@email.com.br";
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@MockBean
	private UserService userService;
	
	@MockBean
	private SingleDigitService singleDigitService;
	
	private User user;
	private HttpHeaders headers;

	@Before
	public void setUp() {
		headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		user = new User(ID, USERNAME, EMAIL);
	}
	
	@Test
	public void whenCalculateSingleDigitAndUserExits_shouldReturnStatusCode201() throws JsonProcessingException {
		
		given(userService.findById(Mockito.anyLong())).willReturn(Optional.ofNullable(user));

		SingleDigitDto digitDto = new SingleDigitDto();
		digitDto.setNumberOfTimes(0);
		digitDto.setValue("11");
		
		HttpEntity<String> entity = createHttpEntity(digitDto);

		ResponseEntity<ModelApiResponse<SingleDigitDto>> response = restTemplate.exchange(createURLWithPort(POST_RESOURCE_URI),
		HttpMethod.POST, entity, new ParameterizedTypeReference<ModelApiResponse<SingleDigitDto>>(){}, 1L);
				
		assertThat(response.getBody().getData().getResult()).isEqualTo(2);
		assertThat(response.getStatusCodeValue()).isEqualTo(201);
	}
	
	@Test
	public void whenCalculateSingleDigitAndUserIsNotPresent_shouldReturnStatusCode404() throws JsonProcessingException {
		
		SingleDigitDto digitDto = new SingleDigitDto();
		digitDto.setNumberOfTimes(0);
		digitDto.setValue("11");
		
		HttpEntity<String> entity = createHttpEntity(digitDto);

		ResponseEntity<ModelApiResponse<SingleDigitDto>> response = restTemplate.exchange(createURLWithPort(POST_RESOURCE_URI),
		HttpMethod.POST, entity, new ParameterizedTypeReference<ModelApiResponse<SingleDigitDto>>(){}, 1L);
				
		assertThat(response.getStatusCodeValue()).isEqualTo(404);
	}
	
	
	private HttpEntity<String> createHttpEntity(SingleDigitDto digitDto) throws JsonProcessingException {
		HttpEntity<String> entity = new HttpEntity<String>(new ObjectMapper().writeValueAsString(digitDto), headers);
		return entity;
	}

	private String createURLWithPort(String uri) {
		return new StringBuffer()
				.append("http://localhost:")
				.append(port)
				.append(uri)
				.toString();
	}
}
