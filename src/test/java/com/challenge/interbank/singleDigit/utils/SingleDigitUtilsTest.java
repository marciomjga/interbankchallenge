package com.challenge.interbank.singleDigit.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SingleDigitUtilsTest {
	
	@Test
	public void whenCalculateValue9875WithNumberOfTimesEqualsZero_shouldReturn2() {
		Integer result = SingleDigitUtils.calculateSingleDigit(9875, 0);
		assertThat(result).isEqualTo(2);
	}
	
	@Test
	public void whenCalculateValue9875WithNumberOfTimesEquals4_shouldReturn8() {
		Integer result = SingleDigitUtils.calculateSingleDigit(9875, 4);
		assertThat(result).isEqualTo(8);
	}
	
	@Test
	public void whenCalculateValue1WithNumberOfTimesEquals0_shouldReturn1() {
		Integer result = SingleDigitUtils.calculateSingleDigit(1, 0);
		assertThat(result).isEqualTo(1);
	}
	
}
