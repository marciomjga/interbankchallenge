package com.challenge.interbank.singleDigit.repositories;

import com.challenge.interbank.singleDigit.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

	private static final String EMAIL = "email@email.com.br";
	private static final String NEW_EMAIL = "new@email.com.br";
	private static final String USER = "user1";

	@Autowired
	private UserRepository userRepository;
	private User user;

	@Before
	public void setUp() {
		user = this.userRepository.save(new User().withUsername(USER).withEmail(EMAIL));
	}
	
	@Test
	public void whenSaveUser_shouldPersistUser() {
		assertThat(user.getUsername()).isNotNull();
		assertThat(user.getEmail()).isNotNull();
		assertThat(user.getUsername()).isEqualTo(USER);
		assertThat(user.getEmail()).isEqualTo(EMAIL);
	}

	@Test
	public void whenFindUserById_thenReturnUser() {
		assertThat(this.userRepository.findById(user.getId())).isNotNull();
	}

	@Test
	public void whenFindByUsername_thenReturnUser() {
		assertThat(this.userRepository.findByUsername(user.getUsername())).isNotNull();
	}

	@Test
	public void whenDeleteUser_shouldRemoveUser() {
		this.userRepository.delete(user);
		assertThat(this.userRepository.existsById(user.getId())).isFalse();
	}

	@Test
	public void whenUpdateUser_shoulddMergeUser() {
		User userUpdated = this.userRepository.save(user.withEmail(NEW_EMAIL));
		assertThat(userUpdated.getId()).isEqualTo(user.getId());
		assertThat(userUpdated.getEmail()).isEqualTo(NEW_EMAIL);
	}
}
