package com.challenge.interbank.singleDigit.impl;

import com.challenge.interbank.singleDigit.model.User;
import com.challenge.interbank.singleDigit.repositories.UserRepository;
import com.challenge.interbank.singleDigit.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {
	
	private static final Long ID = 1234L;
	private static final String USERNAME = "username";
	private static final String EMAIL = "email@email.com";
	
	@MockBean
	private UserRepository userRepository;
	
	@Autowired
	private UserService userService;
	
	private User user;
	
	@Before
	public void setUp() {
		 user = new User(ID, USERNAME, EMAIL);
	}
	
	@Test
	public void whenCreateUser_shouldPersistUser() {
		when(this.userRepository.save(Mockito.any(User.class))).thenReturn(new User());
		User userSaved = userService.save(user);
		assertThat(userSaved).isNotNull();
	}

	@Test
	public void whenFindUserByUsername_shouldReturnUser() {
		when(userRepository.findByUsername(Mockito.anyString())).thenReturn(new User());
		Optional<User> user = userService.findByUsername(USERNAME); 
		assertTrue(user.isPresent());
	}
	
	@Test
	public void whenFindUserById_shouldReturnUser() {
		when(this.userRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(new User()));
		Optional<User> user = userService.findById(ID);
		assertTrue(user.isPresent());
	}
	
	@Test
	public void whenDeleteUser_shouldRemoveUser() {
		User user = new User(ID, USERNAME, EMAIL);
		userService.delete(user);
        verify(userRepository, times(1)).delete(user);
	}
		
	@Test
	public void whenUpdateUser_shoulddMergeAndReturnUser() {
		when(this.userRepository.save(Mockito.any(User.class))).thenReturn(new User());
		User user = userService.update(new User());
		assertThat(user).isNotNull();
	}

}
