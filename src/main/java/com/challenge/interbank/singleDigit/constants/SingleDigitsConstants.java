package com.challenge.interbank.singleDigit.constants;

public class SingleDigitsConstants {
	
	private SingleDigitsConstants() {
	}

	public static final String USER = "User";
	public static final String ERROR_EMAIL_IN_USE = "This email is already in use";
	public static final String USER_NOT_FOUND = "User not found";

}
