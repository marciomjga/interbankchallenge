package com.challenge.interbank.singleDigit.dtos;

import com.challenge.interbank.singleDigit.utils.SingleDigitUtils;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * UserDto
 */
public class UserDto {
	
	private Long id;
	
	@NotNull(message = "The username is required")
	@NotBlank(message = "The username is required")
	private String username;
	
	@NotNull(message = "The email is required")
	@NotBlank(message = "The email is required")
	@Email(message="Invalid email address")
	private String email;
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public UserDto withUsername(String username) {
		this.username = username;
		return this;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public UserDto withEmail(String email) {
		this.email = email;
		return this;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(username, email);
	}
	
	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (object == null || getClass() != object.getClass()) {
			return false;
		}
		UserDto userDto = (UserDto) object;
		return Objects.equals(this.username, userDto.username)
				&& Objects.equals(this.email, userDto.email);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class UserDto {\n");

		sb.append("    username: ").append(SingleDigitUtils.toIndentedString(username)).append("\n");
		sb.append("    email: ").append(SingleDigitUtils.toIndentedString(email)).append("\n");
		sb.append("}");
		return sb.toString();
	}

}
