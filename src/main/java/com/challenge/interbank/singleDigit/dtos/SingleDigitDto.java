package com.challenge.interbank.singleDigit.dtos;

import com.challenge.interbank.singleDigit.utils.SingleDigitUtils;

import java.util.Objects;

public class SingleDigitDto {

	private Long userId;
	private Integer numberOfTimes;
	private String value;
	private Integer result;

	public Integer getNumberOfTimes() {
		return numberOfTimes;
	}

	public void setNumberOfTimes(Integer numberOfTimes) {
		this.numberOfTimes = numberOfTimes;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public Integer getResult() {
		return result;
	}
	
	public void setResult(Integer result) {
		this.result = result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		SingleDigitDto singleDigitDto = (SingleDigitDto) o;
		return Objects.equals(this.numberOfTimes, singleDigitDto.numberOfTimes)
				&& Objects.equals(this.value, singleDigitDto.value)
				&& Objects.equals(this.userId, singleDigitDto.userId)
				&& Objects.equals(this.result, singleDigitDto.result);
	}

	@Override
	public int hashCode() {
		return Objects.hash(numberOfTimes, value, userId);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class SingleDigit {\n");

		sb.append("    numberOfTimes: ").append(SingleDigitUtils.toIndentedString(numberOfTimes)).append("\n");
		sb.append("    value: ").append(SingleDigitUtils.toIndentedString(value)).append("\n");
		sb.append("    userId: ").append(SingleDigitUtils.toIndentedString(userId)).append("\n");
		sb.append("    result: ").append(SingleDigitUtils.toIndentedString(result)).append("\n");
		sb.append("}");
		return sb.toString();
	}
}
