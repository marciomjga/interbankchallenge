package com.challenge.interbank.singleDigit.repositories;

import com.challenge.interbank.singleDigit.model.SingleDigit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SingleDigitRepository extends JpaRepository<SingleDigit, Long> {

}
