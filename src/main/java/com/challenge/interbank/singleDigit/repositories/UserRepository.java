package com.challenge.interbank.singleDigit.repositories;

import com.challenge.interbank.singleDigit.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends JpaRepository<User, Long> {

	@Transactional(readOnly = true)
	User findByUsername(String username);
	
	@Transactional(readOnly = true)
	User findByEmail(String email);
}
