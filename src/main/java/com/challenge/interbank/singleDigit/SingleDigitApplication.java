package com.challenge.interbank.singleDigit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.modelmapper.ModelMapper;

@SpringBootApplication
@EntityScan(basePackages = "com.challenge.interbank.singleDigit")
public class SingleDigitApplication {

	public static void main(String[] args) {
		SpringApplication.run(SingleDigitApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
