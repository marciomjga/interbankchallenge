package com.challenge.interbank.singleDigit.services;

import com.challenge.interbank.singleDigit.model.User;

import java.util.Optional;

public interface UserService {

	/**
	 * Retrieve user by id
	 * 
	 * @param id
	 * @return {@link Optional<User>}
	 */
	Optional<User> findById(Long id);

	/**
	 * Retrieve user by user name
	 * 
	 * @param username
	 * @return {@link Optional<User>}
	 */
	Optional<User> findByUsername(String username);
	
	/**
	 * Retrieve user by email
	 * 
	 * @param username
	 * @return {@link Optional<User>}
	 */
	Optional<User> findByemail(String email);

	/**
	 * Save user
	 * 
	 * @param username
	 * @return {@link User}
	 */
	User save(User user);

	/**
	 * Delete user
	 * 
	 * @param {@link User}
	 * @return {@link Optional<User>}
	 */
	void delete(User user);
	
	/**
	 * Delete user by id
	 * 
	 * @param {@link User}
	 * @return {@link Optional<User>}
	 */
	void deleteById(Long id);

	/**
	 * Update user
	 * 
	 * @param {@link User}
	 * @return {@link Optional<User>}
	 */
	User update(User user);
	
	/**
	 * Check if an user id exists
	 * 
	 * @param {@link User}
	 * @return {@link Optional<User>}
	 */
	Boolean existsById(Long id);

}
