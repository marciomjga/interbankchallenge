package com.challenge.interbank.singleDigit.services;

import com.challenge.interbank.singleDigit.model.SingleDigit;

public interface SingleDigitService {

	/**
	 * Save single digit
	 * 
	 * @param singleDigit
	 * @return {@link SingleDigit}
	 */
	SingleDigit save(SingleDigit singleDigit);
}
