package com.challenge.interbank.singleDigit.services.impl;

import com.challenge.interbank.singleDigit.model.SingleDigit;
import com.challenge.interbank.singleDigit.repositories.SingleDigitRepository;
import com.challenge.interbank.singleDigit.services.SingleDigitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SingleServiceImpl implements SingleDigitService {

	
	@Autowired
	private SingleDigitRepository singleDigitRepository;
	
	
	@Override
	public SingleDigit save(SingleDigit singleDigit) {
		
		return singleDigitRepository.save(singleDigit);
	}
}
