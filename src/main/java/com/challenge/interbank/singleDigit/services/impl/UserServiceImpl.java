package com.challenge.interbank.singleDigit.services.impl;

import com.challenge.interbank.singleDigit.exceptions.NotFoundException;
import com.challenge.interbank.singleDigit.model.User;
import com.challenge.interbank.singleDigit.repositories.UserRepository;
import com.challenge.interbank.singleDigit.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Override
	public Optional<User> findById(Long id) {		
		return userRepository.findById(id);
	}

	@Override
	public Optional<User> findByUsername(String username) {
		return Optional.ofNullable(this.userRepository.findByUsername(username));
	}

	@Override
	public User save(User user) {
		return this.userRepository.save(user);
	}

	@Override
	public void delete(User user) {
		this.userRepository.delete(user);
	}
	
	@Override
	public void deleteById(Long id) {
		if(!userRepository.existsById(id))
			throw new NotFoundException();

		this.userRepository.deleteById(id);
	}

	@Override
	public User update(User user) {
		return this.userRepository.save(user);
	}

	@Override
	public Optional<User> findByemail(String email) {
		return Optional.ofNullable(this.userRepository.findByEmail(email));
	}

	@Override
	public Boolean existsById(Long id) {
		return userRepository.existsById(id);
	}
	
}
