package com.challenge.interbank.singleDigit.utils;

public class SingleDigitUtils {
	
	private SingleDigitUtils() {
		super();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	public static String toIndentedString(Object object) {
		if (object == null) {
			return "null";
		}
		return object.toString().replace("\n", "\n    ");
	}
	
	/**
	 * Convert the given integer valeu to only digit
	 * 
	 *  If x has just only digit, then your only digit is x.
	 *  Otherwise, the single digit of x is equal to the single digit of sum of the digits of x.
	 *  
	 *  @param value represented an integer. 1 <= n <= 101000000
	 *  @param numberOfTimes an integer representing the number of times the concatenation
	 */
	public static Integer calculateSingleDigit(int value,  int numberOfTimes) {
		int result = 0;
		int n = value;
		while(n > 0) {
			result += n % 10;
			n /= 10;
		}		
		
		int k = (numberOfTimes == 0) ? 1 : numberOfTimes;
		
		int length = (int) (Math.log10(result*k) + 1);
		
		if(length > 1) return calculateSingleDigit(result, numberOfTimes);
		
		return result*k ;
	}

}
