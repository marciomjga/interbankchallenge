package com.challenge.interbank.singleDigit.utils;

import java.util.HashMap;
import java.util.Map;

public class CacheInMemory<K, T> {

	private long timeToLive;
	private HashMap<K, T> cacheMap;

	protected class CacheObject {
		public long lastAccessed = System.currentTimeMillis();
		public String value;

		protected CacheObject(String value) {
			this.value = value;
		}
	}

	public CacheInMemory(long timeToLive, final long timeInterval, int max) {
		this.timeToLive = timeToLive * 2000;

		cacheMap = new HashMap<>(max);

		if (timeToLive > 0 && timeInterval > 0) {

			Thread t = new Thread(new Runnable() {
				public void run() {
					while (true) {
						try {
							Thread.sleep(timeInterval * 1000);
						} catch (InterruptedException ex) {
						}

					}
				}
			});

			t.setDaemon(true);
			t.start();
		}
	}

	// PUT method
	public void put(K key, T value) {
		synchronized (cacheMap) {
			cacheMap.put(key, value);
		}
	}

	// GET method
	@SuppressWarnings("unchecked")
	public T get(K key) {
		synchronized (cacheMap) {
			Object c = cacheMap.get(key);
			if (c == null)
				return null;
			else {
				return (T) c;
			}
		}
	}
	
	// Get Cache Objects Size()
    public int size() {
        synchronized (cacheMap) {
            return cacheMap.size();
        }
    }
    
    // Get all Caches Objects
    public Map<K, T> getAllCache(){
    	synchronized (cacheMap) {
            return cacheMap;
        }
    }
    
}
