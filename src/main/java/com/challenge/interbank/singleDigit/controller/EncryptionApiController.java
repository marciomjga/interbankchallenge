package com.challenge.interbank.singleDigit.controller;

import com.challenge.interbank.singleDigit.model.ModelApiResponse;
import com.challenge.interbank.singleDigit.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/v1/encryption")
public class EncryptionApiController {
	
	/**
	 * Create User
	 * 
	 * The service should be able to add a new user by the POST request at /users.
	 * The user JSON is sent in the request body. If an user with the same email
	 * already exists then the HTTP response code should be 400, otherwise, the
	 * response code should be 201.
	 * 
	 * @param result
	 * @return {@link ResponseEntity < ModelApiResponse < User >>}
	 */
	@PostMapping
	public ResponseEntity<String> createUser(@RequestBody @Valid String publicKey,
                                             BindingResult result) {

		return ResponseEntity.status(CREATED).body(publicKey);
	}

}
