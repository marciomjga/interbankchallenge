package com.challenge.interbank.singleDigit.controller;

import com.challenge.interbank.singleDigit.dtos.UserDto;
import com.challenge.interbank.singleDigit.model.ModelApiResponse;
import com.challenge.interbank.singleDigit.model.User;
import com.challenge.interbank.singleDigit.services.UserService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.modelmapper.ModelMapper;

import javax.validation.Valid;
import java.util.Optional;

import static com.challenge.interbank.singleDigit.constants.SingleDigitsConstants.USER_NOT_FOUND;
import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/v1/users")
@Api(value = "user-api-controller")
public class UserApiController {

	private final UserService userService;

	@Autowired
	public UserApiController(UserService userService) {
		this.userService = userService;
	}

	@Autowired
	private ModelMapper modelMapper;

	/**
	 * Create User
	 * 
	 * The service should be able to add a new user by the POST request at /users.
	 * The user JSON is sent in the request body. If an user with the same email
	 * already exists then the HTTP response code should be 400, otherwise, the
	 * response code should be 201.
	 * 
	 * @param userDTO
	 * @return {@link ResponseEntity <ModelApiResponse<User>>}
	 */
	@ApiOperation(value = "Create user", nickname = "createUser", notes = "", response = ModelApiResponse.class, tags = {
			"user-api-controller", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "successful operation", response = ModelApiResponse.class),
			@ApiResponse(code = 400, message = "Invalid username supplied", response = ModelApiResponse.class) })
	@PostMapping
	public ResponseEntity<ModelApiResponse<User>> createUser(
			@ApiParam(value = "Created user object", required = true) @RequestBody @Valid UserDto userDto,
			BindingResult result) {

		ModelApiResponse<User> userResponse = new ModelApiResponse<>();

		if (result.hasErrors()) {
			result.getAllErrors().forEach(error -> userResponse.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.status(BAD_REQUEST).body(userResponse);
		}

		userResponse.setData(userService.save(modelMapper.map(userDto, User.class)));

		return ResponseEntity.status(CREATED).body(userResponse);
	}

	/**
	 * Get User By Id
	 * 
	 * The service should be able to return the JSON of the user which are performed
	 * by the user ID by the GET request at /users/{userId}. If the requested users
	 * does not exist then HTTP response code should be 404, otherwise, the response
	 * code should be 200.
	 * 
	 * @param userId
	 * @return {@link ResponseEntity <ModelApiResponse<User>>}
	 */
	@ApiOperation(value = "Get user by user id", nickname = "getUserById", notes = "", response = User.class, tags = {
			"user-api-controller", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "successful operation", response = ModelApiResponse.class),
			@ApiResponse(code = 400, message = "Invalid username supplied", response = ModelApiResponse.class),
			@ApiResponse(code = 404, message = "User not found", response = ModelApiResponse.class) })
	@GetMapping(path = "/{userId}")
	public ResponseEntity<ModelApiResponse<User>> getUserById(
			@ApiParam(value = "userId that needs to be fetched", required = true) @PathVariable("userId") Long userId) {
		ModelApiResponse<User> userResponse = new ModelApiResponse<>();
		Optional<User> user = userService.findById(userId);
		if (!user.isPresent()) {
			userResponse.getErrors().add(USER_NOT_FOUND);
			return ResponseEntity.status(NOT_FOUND).body(userResponse);
		}
		userResponse.setData(user.get());
		return ResponseEntity.status(OK).body(userResponse);
	}

	/**
	 * Get User By username
	 * 
	 * The service should be able to return the JSON of the user which are performed
	 * by the username by the GET request at /findByName/{username}. If the
	 * requested user does not exist then HTTP response code should be 404,
	 * otherwise, the response code should be 200.
	 * 
	 * @param username
	 * @return {@link ResponseEntity <ModelApiResponse<User>>}
	 */
	@ApiOperation(value = "Get user by user name", nickname = "getUserByUsername", notes = "", response = User.class, tags = {
			"user-api-controller", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "successful operation", response = ModelApiResponse.class),
			@ApiResponse(code = 400, message = "Invalid username supplied", response = ModelApiResponse.class),
			@ApiResponse(code = 404, message = "User not found", response = ModelApiResponse.class) })
	@GetMapping(path = "/findByName/{username}")
	public ResponseEntity<ModelApiResponse<User>> getUserByUsername(
			@ApiParam(value = "username that needs to be fetched", required = true) @PathVariable("username") String username) {
		ModelApiResponse<User> userResponse = new ModelApiResponse<>();
		Optional<User> user = userService.findByUsername(username);
		if (!user.isPresent()) {
			userResponse.getErrors().add(USER_NOT_FOUND);
			return ResponseEntity.status(NOT_FOUND).body(userResponse);
		}
		userResponse.setData(user.get());
		return ResponseEntity.status(OK).body(userResponse);
	}

	/**
	 * Delete User by id
	 * 
	 * The service should be able to remove the user which are performed by the id
	 * the by the DELETE request at /users/{id}. The HTTP response code should be
	 * 200.
	 * 
	 * @param userId
	 * @return {@link ResponseEntity <Void>}
	 */
	@ApiOperation(value = "Delete user", nickname = "deleteUser", notes = "", response = ModelApiResponse.class, tags = {
			"user-api-controller", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "successful operation", response = ModelApiResponse.class),
			@ApiResponse(code = 400, message = "Invalid username supplied", response = ModelApiResponse.class),
			@ApiResponse(code = 404, message = "User not found", response = ModelApiResponse.class) })
	@DeleteMapping(path = "/{userId}")
	public ResponseEntity<Void> deleteUser(
			@ApiParam(value = "The user ID that needs to be deleted", required = true) @PathVariable("userId") Long userId) {
		userService.deleteById(userId);
		return ResponseEntity.status(OK).build();
	}

	/**
	 * Update the user
	 * 
	 * The service should be able to update the user by the PUT request at
	 * /users/{userId}. The user JSON is sent in the request body. If the user with
	 * the id does not exist then the response code should be 404, otherwise the
	 * response code should be 200.
	 * 
	 * @param userId
	 * @param        {@link UserDto}
	 * @return {@link ResponseEntity <Void>}
	 */
	@ApiOperation(value = "Updated user", nickname = "updateUser", notes = "", response = ModelApiResponse.class, tags = {
			"user-api-controller", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "successful operation", response = ModelApiResponse.class),
			@ApiResponse(code = 400, message = "Invalid user supplied", response = ModelApiResponse.class),
			@ApiResponse(code = 404, message = "User not found", response = ModelApiResponse.class) })
	@PutMapping(path = "/{userId}")
	public ResponseEntity<ModelApiResponse<User>> updateUser(
            @ApiParam(value = "user Id that need to be updated", required = true) @PathVariable("userId") Long userId,
            @ApiParam(value = "User", required = true) @RequestBody @Valid UserDto userDto, BindingResult result) {
		ModelApiResponse<User> userResponse = new ModelApiResponse<>();
		Optional<User> user = userService.findById(userId);
		if (!user.isPresent()) {
			return ResponseEntity.status(NOT_FOUND).build();
		}
		userDto.setId(user.get().getId());
		userResponse.setData(userService.update(modelMapper.map(userDto, User.class)));

		return ResponseEntity.status(OK).build();
	}

}
