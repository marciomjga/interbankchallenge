package com.challenge.interbank.singleDigit.controller;

import com.challenge.interbank.singleDigit.dtos.SingleDigitDto;
import com.challenge.interbank.singleDigit.model.ModelApiResponse;
import com.challenge.interbank.singleDigit.model.SingleDigit;
import com.challenge.interbank.singleDigit.model.User;
import com.challenge.interbank.singleDigit.services.SingleDigitService;
import com.challenge.interbank.singleDigit.services.UserService;
import com.challenge.interbank.singleDigit.utils.CacheInMemory;
import com.challenge.interbank.singleDigit.utils.SingleDigitUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.Optional;

import static com.challenge.interbank.singleDigit.constants.SingleDigitsConstants.USER_NOT_FOUND;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/v1/calculate")
@Api(value = "single-digit-api-controller")
public class SingleDigitApiController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private SingleDigitService singleDigitService;
	
	private CacheInMemory<String, SingleDigitDto> cache = new CacheInMemory<>(1000, 100000, 100000);

	/**
	 * Calculate the single digit 
	 * 
	 * The service should be able to calculate the single digit and link 
	 * an user by the POST request at /calculate/{userId}.
	 * The values of JSON is sent in the request body. If an user does not
	 * exists then the HTTP response code should be 404, otherwise, the
	 * response code should be 201.
	 * 
	 * @param userId
	 * @param {{@link SingleDigitDto}
	 * @return {@link ResponseEntity <ModelApiResponse<SingleDigitDto>>}
	 */
	
	@PostMapping(path = "/{userId}")
	public ResponseEntity<ModelApiResponse<SingleDigitDto>> calculateSingleDigit(@ApiParam(value = "Created user object" ,required=true ) @PathVariable("userId") Long userId,
                                                                                 @RequestBody @Valid SingleDigitDto singleDigitDto, BindingResult bindingResult) {
		
		ModelApiResponse<SingleDigitDto> response = new ModelApiResponse<>();
		
		Optional<User> user = userService.findById(userId);
		if(!user.isPresent()) {
			response.getErrors().add(USER_NOT_FOUND);
			return ResponseEntity.status(NOT_FOUND).body(response);
		}
		
		Integer result = null;
		
		if(cache.get(singleDigitDto.getValue()) == null) {
			result = SingleDigitUtils.calculateSingleDigit(Integer.parseInt(singleDigitDto.getValue()), singleDigitDto.getNumberOfTimes());
		}
		
		SingleDigit singleDigit = new SingleDigit();
		singleDigit.setUser(user.get());
		singleDigit.setValue(singleDigitDto.getValue());
		singleDigit.setNumberOfTimes(singleDigitDto.getNumberOfTimes());
		
		singleDigit.setResult(result == null ? cache.get(singleDigitDto.getValue()).getResult() : result);
		singleDigitDto.setResult(singleDigit.getResult());
		singleDigitDto.setUserId(userId);
		
		singleDigitService.save(singleDigit);
		
		response.setData(singleDigitDto);
		
		if(cache.size() <= 10) {
			cache.put(singleDigitDto.getValue(), singleDigitDto);
		}
			
		return ResponseEntity.status(CREATED).body(response);
	}
	
	/**
	 * Get all single digit calculate in cache 
	 * 
	 * The service should be able to get all single digit calculate in cache 
	 * an user by the GET request at /calculate/cache.
	 * The values of JSON is sent in the request body. 
	 * The HTTP response code should be 200
	 *  
	 * @param userId
	 * @param {{@link SingleDigitDto}
	 * @return {@link ResponseEntity <ModelApiResponse<SingleDigitDto>>}
	 */
	@ApiOperation(value = "Get all single digit calculate in cache", nickname = "getCache", notes = "", response = SingleDigit.class, responseContainer = "List", tags={ "single-digit-api-controller", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = SingleDigit.class, responseContainer = "List") })
	@GetMapping(path = "/cache")
	public ResponseEntity<Map<String,SingleDigitDto>> getCache() {
		return ResponseEntity.status(HttpStatus.OK).body(cache.getAllCache());
	}
	
}
