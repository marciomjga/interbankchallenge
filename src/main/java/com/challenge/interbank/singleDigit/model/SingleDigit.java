package com.challenge.interbank.singleDigit.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * SingleDigit
 */
@Entity
@Table(name = "single_digit")
public class SingleDigit implements Serializable {

	private static final long serialVersionUID = 7572998367964859579L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "number_of_times")
	private Integer numberOfTimes = null;
	
	@Column(name = "value")
	private String value = null;
	
	@Column(name = "result")
	private Integer result = null;
	
	@ManyToOne(fetch= FetchType.LAZY)
	@JoinColumn(name="user_id")
	private User user = null;
	
	public SingleDigit withId(Long id) {
		this.id = id;
		return this;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SingleDigit numberOfTimes(Integer numberOfTimes) {
		this.numberOfTimes = numberOfTimes;
		return this;
	}

	public Integer getNumberOfTimes() {
		return numberOfTimes;
	}

	public void setNumberOfTimes(Integer numberOfTimes) {
		this.numberOfTimes = numberOfTimes;
	}

	public SingleDigit value(String value) {
		this.value = value;
		return this;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public SingleDigit result(Integer result) {
		this.result = result;
		return this;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public SingleDigit withUser(User user) {
		this.user = user;
		return this;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (object == null || getClass() != object.getClass()) {
			return false;
		}
		SingleDigit singleDigit = (SingleDigit) object;
		return Objects.equals(this.numberOfTimes, singleDigit.numberOfTimes)
				&& Objects.equals(this.value, singleDigit.value) && Objects.equals(this.result, singleDigit.result)
				&& Objects.equals(this.user, singleDigit.user);
	}

	@Override
	public int hashCode() {
		return Objects.hash(numberOfTimes, value, result, user);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class SingleDigit {\n");

		sb.append("    numberOfTimes: ").append(toIndentedString(numberOfTimes)).append("\n");
		sb.append("    value: ").append(toIndentedString(value)).append("\n");
		sb.append("    result: ").append(toIndentedString(result)).append("\n");
		sb.append("    user: ").append(toIndentedString(user)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(Object object) {
		if (object == null) {
			return "null";
		}
		return object.toString().replace("\n", "\n    ");
	}
}
