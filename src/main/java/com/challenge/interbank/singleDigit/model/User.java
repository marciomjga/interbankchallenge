package com.challenge.interbank.singleDigit.model;

import com.challenge.interbank.singleDigit.utils.SingleDigitUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * User
 */
@Entity
@Table(name = "user")
public class User implements Serializable {

	private static final long serialVersionUID = 1186877350595335698L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id = null;

	@Column(name = "username")
	private String username = null;
	
	@Column(name = "email")
	private String email = null;

	@OneToMany(mappedBy = "user", targetEntity = SingleDigit.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<SingleDigit> singleDigits;

	public User withId(Long id) {
		this.id = id;
		return this;
	}
	
	public User() {
		super();
	}

	public User(Long id, @NotNull(message = "The username is required") String username,
			@NotNull(message = "The email is required") String email) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User withUsername(String username) {
		this.username = username;
		return this;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public User withEmail(String email) {
		this.email = email;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public User withSingleDigits(Set<SingleDigit> singleDigits) {
		this.singleDigits = singleDigits;
		return this;
	}

	public Set<SingleDigit> getSingleDigits() {
		return singleDigits;
	}

	public void setSingleDigits(Set<SingleDigit> singleDigits) {
		this.singleDigits = singleDigits;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (object == null || getClass() != object.getClass()) {
			return false;
		}
		User user = (User) object;
		return Objects.equals(this.id, user.id) && Objects.equals(this.username, user.username)
				&& Objects.equals(this.email, user.email);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, username, email);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class User {\n");

		sb.append("    id: ").append(SingleDigitUtils.toIndentedString(id)).append("\n");
		sb.append("    username: ").append(SingleDigitUtils.toIndentedString(username)).append("\n");
		sb.append("    email: ").append(SingleDigitUtils.toIndentedString(email)).append("\n");
		sb.append("}");
		return sb.toString();
	}
}
