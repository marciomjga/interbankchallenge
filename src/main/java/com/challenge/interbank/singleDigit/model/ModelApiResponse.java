package com.challenge.interbank.singleDigit.model;

import java.util.ArrayList;
import java.util.List;

/**
 * ModelApiResponse
 */
public class ModelApiResponse<T>   {

	private T data;
	private List<String> errors;

	public ModelApiResponse() {
		super();
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public List<String> getErrors() {
		if (this.errors == null) {
			this.errors = new ArrayList<>();
		}
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
}

